﻿Services.StartTransaction "Speakers"
Browser("Advantage Shopping").Page("Advantage Shopping").Link("SPEAKERS Shop Now").Click
Services.EndTransaction "Speakers"


Browser("Advantage Shopping").Page("Advantage Shopping").WebElement("SPEAKERS").Check CheckPoint("SPEAKERS") @@ script infofile_;_ZIP::ssf2.xml_;_

Services.StartTransaction "Speaker_Image"
Browser("Advantage Shopping").Page("Advantage Shopping").Image("fetchImage?image_id=4200").Click @@ script infofile_;_ZIP::ssf3.xml_;_
Services.EndTransaction "Speaker_Image"

' Contact form
Browser("Advantage Shopping").Page("Advantage Shopping").WebButton("save_to_cart").Check CheckPoint("save_to_cart") @@ script infofile_;_ZIP::ssf4.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Image("fetchImage?image_id=4200").Check CheckPoint("fetchImage?image_id=4200") @@ script infofile_;_ZIP::ssf5.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Image("fetchImage?image_id=4200").Click @@ script infofile_;_ZIP::ssf6.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Link("HOME").Click @@ script infofile_;_ZIP::ssf7.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Link("MICE Shop Now").Click @@ script infofile_;_ZIP::ssf8.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Image("fetchImage?image_id=4200").Click @@ script infofile_;_ZIP::ssf9.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").WebElement("HP USB 3 BUTTON OPTICAL").Check CheckPoint("HP USB 3 BUTTON OPTICAL MOUSE") @@ script infofile_;_ZIP::ssf10.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Link("HOME").Click @@ script infofile_;_ZIP::ssf11.xml_;_


Services.StartTransaction "Contact_Us"
Browser("Advantage Shopping").Page("Advantage Shopping").Link("CONTACT US").Click 
Services.EndTransaction "Contact_Us"

Browser("Advantage Shopping").Page("Advantage Shopping").WebList("categoryListboxContactUs").Select DataTable("Category", dtGlobalSheet) @@ script infofile_;_ZIP::ssf13.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").WebList("productListboxContactUs").Select DataTable("Item", dtGlobalSheet) @@ script infofile_;_ZIP::ssf14.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").WebEdit("emailContactUs").Set DataTable("EMail", dtGlobalSheet) @@ script infofile_;_ZIP::ssf15.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").WebEdit("subjectTextareaContactUs").Set "What is the battery life for this model.  Thanks. " @@ script infofile_;_ZIP::ssf16.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").WebButton("send_btnundefined").Click @@ script infofile_;_ZIP::ssf17.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").WebElement("Thank you for contacting").Check CheckPoint("Thank you for contacting Advantage support.") @@ script infofile_;_ZIP::ssf18.xml_;_
Browser("Advantage Shopping").Page("Advantage Shopping").Link("CONTINUE SHOPPING").Click @@ script infofile_;_ZIP::ssf19.xml_;_
